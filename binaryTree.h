
#include "treeException.h"
#include "treeNode.h"
#include <iostream>


// DT is the data type for treeItemType
class BinaryTree
{
public:
    BinaryTree();
    BinaryTree(DT);
    BinaryTree(const BinaryTree&) throw(TreeException);
    //BinaryTree(BinaryTree*);
    bool isEmpty();
    DT getRootData() throw(TreeException);
    void setRootData(DT) throw(TreeException);
    void insert(DT);
    void addData(DT);

    void displayTree() throw(TreeException);
    


private:
    TreeNode*   root;

    typedef void (BinaryTree::*funPtr)(TreeNode*);

    void displayTree_prog(TreeNode*);
    void addData_prog(TreeNode*&, DT);
    void inorder(TreeNode*, funPtr); 
    void preorder(TreeNode*, funPtr); 
    void postorder(TreeNode*, funPtr); 
    void printNode(TreeNode*);
    void copyBranch(TreeNode*&, const TreeNode*);


};
