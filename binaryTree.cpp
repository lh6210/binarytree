
#include "binaryTree.h"

BinaryTree::BinaryTree():root{NULL}
{}

BinaryTree::BinaryTree(DT data):root{new TreeNode(data)}
{
}

BinaryTree::BinaryTree(const BinaryTree& t1) throw(TreeException)
{
    if (!(t1.root))
        throw TreeException("Empty tree!");

    // t1.root is a pointer to a TreeNode
    copyBranch(root, t1.root); 
}

void BinaryTree::copyBranch(TreeNode*& dstNd, const TreeNode* srcNd)
{
    if (!srcNd)
        return;

    dstNd = new TreeNode(*srcNd);
    copyBranch(dstNd->left, srcNd->left);
    copyBranch(dstNd->right, srcNd->right);
}




bool BinaryTree::isEmpty()
{
    if (!root)
        return true;

    return false;
}

DT BinaryTree::getRootData() throw(TreeException)
{
    if (!root)
        throw TreeException("Empty tree");

    return root->item;
}

void BinaryTree::setRootData(DT d) throw(TreeException)
{
    if (!root)
        throw TreeException("Empty tree");

    root->item = d;
    return;
}


void BinaryTree::addData(DT t)
{
    return addData_prog(root, t);
}


void BinaryTree::addData_prog(TreeNode*& nd, DT t)
{
    if (!nd)
    {
        nd = new TreeNode(t);
        return;
    }
    
    if (nd->item < t)
        return addData_prog(nd->right, t);
    else
        return addData_prog(nd->left, t);
}

void BinaryTree::displayTree() throw(TreeException)
{
    if (!root)
        throw TreeException("Empty tree!");

    //displayTree_prog(root);
    cout << "Inorder: " << endl;
    inorder(root, &BinaryTree::printNode);
    cout << endl;
    cout << "Preorder:" << endl;
    preorder(root, &BinaryTree::printNode);
    cout << endl;
    cout << "Postorder:" << endl;
    postorder(root, &BinaryTree::printNode);
    cout << endl;
}

/*
void BinaryTree::displayTree_prog(TreeNode* nd)
{
    if (!nd)
        return;

    displayTree_prog(nd->left);
    printNode(nd);
    displayTree_prog(nd->right);

}
*/

void BinaryTree::printNode(TreeNode* nd) 
{
    cout << nd->getItem() << " ";
}


void BinaryTree::inorder(TreeNode* nd, funPtr p)
{
    if (!nd)
        return;
    
    inorder(nd->left, p);
    (this->*p)(nd);
    inorder(nd->right, p);
}


void BinaryTree::preorder(TreeNode* nd, funPtr p)
{
    if (!nd)
        return;
    
    (this->*p)(nd);
    preorder(nd->left, p);
    preorder(nd->right, p);
}

void BinaryTree::postorder(TreeNode* nd, funPtr p)
{
    if (!nd)
        return;

    postorder(nd->left, p);
    postorder(nd->right, p);
    (this->*p)(nd);
}

        



