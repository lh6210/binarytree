
typedef int DT;

class TreeNode
{
private:
    TreeNode(){};
    TreeNode(const DT& nodeItem, 
             TreeNode* l = NULL, TreeNode* r = NULL):item{nodeItem}, left{l}, right{r}
    {
    }
    TreeNode(const TreeNode& nd):item{nd.item}, left{NULL}, right{NULL}
    {
    }

    DT getItem(){ return item;}
    DT              item;
    TreeNode*       left;
    TreeNode*       right;
    friend class BinaryTree;
};

