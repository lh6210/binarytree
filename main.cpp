
#include "binaryTree.h"

using namespace std;

int main()
{
    BinaryTree* bst = new BinaryTree(2);
    int tmp = bst->getRootData();
    cout << tmp << endl;
    bst->setRootData(12);
    tmp = bst->getRootData();
    cout << tmp << endl;
    bst->addData(14);
    bst->addData(15);
    bst->addData(6);
    bst->addData(13);
    bst->addData(3);
    bst->addData(7);
    bst->displayTree();

    BinaryTree* b2 = new BinaryTree(*bst);
    cout << "b2 is created by copy constructor:" << endl;
    b2->displayTree();
    
    bst->displayTree();

    return 0;
}



